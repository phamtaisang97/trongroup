<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('gioi-thieu', [\App\Http\Controllers\AboutController::class, 'index'])->name('gioi-thieu');
Route::get('du-an', [\App\Http\Controllers\BlogController::class, 'project'])->name('du-an');
Route::get('tin-tuc', [\App\Http\Controllers\BlogController::class, 'index'])->name('tin-tuc');
Route::get('/{alias}', [\App\Http\Controllers\BlogController::class, 'detail'])->name('blog');
