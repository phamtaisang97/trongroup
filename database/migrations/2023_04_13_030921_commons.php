<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('commons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo_header', 255);
            $table->string('logo_footer', 255);
            $table->string('phone', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('link_facebook', 255)->nullable();
            $table->string('link_youtube', 255)->nullable();
            $table->string('link_google', 255)->nullable();
            $table->string('link_twitter', 255)->nullable();
            $table->string('link_linkedin', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('commons');
    }
};
