@include('website.common.header')
<main>
     @yield('content')
</main>
@include('website.common.footer')
