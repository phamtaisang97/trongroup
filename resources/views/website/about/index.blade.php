@extends('website.layout.master')
@section('content')
<div class="container">
    <div class="row">
       <div class="col-12">
            <nav class="nav-breadcrumb" style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Giới thiệu</li>
                </ol>
            </nav> 
       </div>
    </div>
    <div class="row about">
        <div class="col-lg-9">
            <h1>Giới thiệu chung</h1>
            <div class="description">
                <p>
                    Lien Son Thang Long Joint Stock Company (LISOCON) is a very reputation in supplying various kinds: dry container, office container, reefer container, panel house as housing, administrative office, industrial steel buildings. The product of LISOCON was powerful application in the field of con- struction, industry, and logistics and has been confirmed by reputation, quality with customers. Currently we are ready to supply products and services throughout the country, and oriented to international, provide international export. We aim to develop products conveniently in many areas of social life with philosophy “solution is constantly increasing”.
                </p>
                <img src="/images/about.gif" alt="about">
                <p>Lien Son Thang Long Joint Stock Company (LISOCON) is a very reputation in supplying various kinds: dry container, office container, reefer container, panel house as housing, administrative office, industrial steel buildings. The product of LISOCON was powerful application in the field of con- struction, industry, and logistics and has been confirmed by reputation, quality with customers. Currently we are ready to supply products and services throughout the country, and oriented to international, provide international export. We aim to develop products conveniently in many areas of social life with philosophy “solution is constantly increasing”.</p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="box-right">
                <label>Dự án nổi bật</label>
                <a href="" class="blog">
                    <img src="/images/da.gif" alt="da">
                    <h3>Lien Son Thang Long</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </a>
            </div>
            <div class="box-right">
                <label>tin tức nổi bật</label>
                <a href="" class="blog">
                    <img src="/images/tt.gif" alt="da">
                    <h3>biệt thự cao cấp 2023</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </a>
                <a href="" class="blog">
                    <img src="/images/bt.gif" alt="da">
                    <h3>biệt thự cao cấp 2024</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
