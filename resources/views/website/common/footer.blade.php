
<section class="register-email" style="background: url('/images/bg-contact.gif') no-repeat; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                Đăng ký nhận email để nhận tư vấn
            </div>
            <div class="col-lg-7">
                <form action="" method="POST" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="email" name="email" class="form-control" id="emailContact" placeholder="Nhập địa chỉ email"
                    required>
                    <button type="submit" id="submitContact" class="btn btn-primary">Gửi</button>
                </form>
            </div>
        </div>
    </div>
</section>
<footer>
 <div class="bg-dark d-none d-sm-block">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        @if(!empty($menus))
                            @foreach($menus as $key => $menu)
                                <li class="nav-item">
                                    <a class="nav-link {{ $menu->alias == $page->alias ? 'active' : '' }}" aria-current="page" href="{{ $menu->alias }}">{{ $menu->title }}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </div>
 </div>
 <div class="container-fluid">
    <div class="row">
        <div class="col-lg-5 d-flex align-items-center">
            <div class="left">
                <div class="logo">
                    <a href="/"><img src="/images/logo.gif" alt="logo"></a>
                </div>
                <div class="icons">
                    <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-google" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </div>
                <div class="info">
                    <p>CÔNG TY TNHH TRON GROUP</p>
                    <ul>
                        <li>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <span>Add: Tầng 2, số 2 ngõ 678 đường Láng, P. Láng Thượng,
                                Q. Đống Đa, Hà Nội</span>
                        </li>
                        <li>
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <span>Hotline: 090 172 1000 - 090 170 6000</span>
                        </li>
                        <li>
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <span>E-mail: Trongroup@gmail.com</span>
                        </li>
                        <li>
                            © Copyright by Trongroup
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <iframe width="100%" src="https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d3724.4072724893263!2d105.80200147605446!3d21.016383988201984!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2s!5e0!3m2!1svi!2s!4v1682751610893!5m2!1svi!2s" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </div>
 </div>
</footer>
<!-- Swiper JS -->
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

@yield('script')
</body>
</html>

