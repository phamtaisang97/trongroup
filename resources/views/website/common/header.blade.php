<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> {{ isset($page) ? $page['meta_title'] : '' }} </title>
    <meta name="description" content="{{ isset($page) ? $page['meta_description'] : '' }}" />
    <meta name="keywords" content="{{ isset($page) ? $page['meta_keyword'] : '' }}" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{ url('style.css') }}" rel="stylesheet">
    <link href="{{ url('header.css') }}" rel="stylesheet">
    <link href="{{ url('footer.css') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <link href="{{ url('font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" media="all and (device-width: 768px) and (device-height: 1024px) and (orientation:portrait)" href="ipad-portrait.css" />
    <link rel="stylesheet" media="all and (device-width: 768px) and (device-height: 1024px) and (orientation:landscape)" href="ipad-landscape.css" />
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/228006f19a.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <link rel='stylesheet' href='https://cdn.rawgit.com/daneden/animate.css/v3.1.0/animate.min.css'>
    @yield('head')
</head>
<body>
    <header>
        <div class="top d-none d-sm-block">
            <div class="container">
                <div class="d-lg-flex justify-content-between">
                    <div>
                        <a href="">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <span>Hotline: 090 172 1000 - 090 170 6000</span>
                        </a>
                        <a href="">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <span>Email: Trongroup@gmail.com</span>
                    </div>
                    <div class="icons d-flex">
                        <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-google" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        <a href=""><img src="/images/icons/vn.gif" alt="vn"></a>
                        <a href=""><img src="/images/icons/en.gif" alt="vn"></a>
                    </div>
                </div>
            </div>
        </div>
     
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand" href="/">
                    <img src="/images/logo.gif" class="logo" alt="trongroup">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
                <div class="collapse navbar-collapse d-lg-flex justify-content-between" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        @if(!empty($menus))
                            @foreach($menus as $key => $menu)
                                <li class="nav-item">
                                    <a class="nav-link {{ $menu->alias == $page->alias ? 'active' : '' }}" aria-current="page" href="{{ $menu->alias }}">{{ $menu->title }}</a>
                                </li>
                            @endforeach
                        @endif
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                              Dropdown
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <li><a class="dropdown-item" href="#">Action</a></li>
                              <li><a class="dropdown-item" href="#">Another action</a></li>
                              <li><hr class="dropdown-divider"></li>
                              <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                          </li>
                    </ul>
                    <a href=""><i class="fa fa-search" aria-hidden="true"></i></a>
                </div>
            </div>
        </nav>

    </header>
