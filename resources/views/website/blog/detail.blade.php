@extends('website.layout.master')
@section('head')
    <link href="{{ url('service.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section class="title-header" style="@if($isMobile == 1) background: url('/images/Hero_mb.png') no-repeat; @else background: url('/images/hero.png') no-repeat; @endif background-size: 100%;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center mr-auto">{{ $blog['title'] }}</h1>
                    <p class="txt-category text-center">
                        Trong <b style="color: #74FAEA">{{$blog->category->title}}</b>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="bg-white">
            <div class="description">
                <p>
                    Ngày tạo: {{$blog['created_at']}}
                </p>
                {!! $blog['description'] !!}
            </div>
        </div>
    </div>
@endsection

<style>
    .title-header h1 {
        font-size: 48px;
        line-height: 84px;
        margin: auto;
    }

    .txt-category {
        font-style: normal;
        font-weight: 600;
        font-size: 16px;
        line-height: 19px;
        letter-spacing: 2px;
        text-transform: capitalize;
    }

    .bg-white {
        padding: 16px;
        margin: auto;
        transform: translateY(-46px);
        max-width: 880px;
        border-radius: 16px;
    }
</style>
