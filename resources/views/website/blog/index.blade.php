@extends('website.layout.master')
@section('content')
<div class="container">
    <div class="row">
       <div class="col-12">
            <nav class="nav-breadcrumb" style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Tin tức</li>
                </ol>
            </nav> 
       </div>
    </div>
    <div class="row about">
        <div class="col-lg-9">
           <div class="row">
                <div class="col-lg-12">
                    <a href="" class="blog news d-flex">
                        <img class="mr-3" src="/images/da.gif" alt="da" width="275px">
                        <div class="mr-auto">
                            <h3>Lien Son Thang Long</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-12">
                    <a href="" class="blog news d-flex align-item-center">
                        <img src="/images/da.gif" alt="da" width="275px">
                        <div class="mr-auto">
                            <h3>Lien Son Thang Long</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="box-right">
                <label>Dự án nổi bật</label>
                <a href="" class="blog">
                    <img src="/images/da.gif" alt="da">
                    <h3>Lien Son Thang Long</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </a>
            </div>
            <div class="box-right">
                <label>tin tức nổi bật</label>
                <a href="" class="blog">
                    <img src="/images/tt.gif" alt="da">
                    <h3>biệt thự cao cấp 2023</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </a>
                <a href="" class="blog">
                    <img src="/images/bt.gif" alt="da">
                    <h3>biệt thự cao cấp 2024</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
