@section('head')
    <link href="{{ url('home.css') }}" rel="stylesheet">
@endsection
@extends('website.layout.master')
@section('content')
   <section class="slide">
      <div class="swiper slideSwiper">
          <div class="swiper-wrapper">
            <div class="swiper-slide" style="background: url('/images/slide1.gif') no-repeat">
              <div class="container">
                <div class="info">
                  <h1>Protection of architectural designs
                    as intellectual property</h1>
                    <a class="btn" href="">Xem thêm</a>
                </div>
              </div>
            </div>
            <div class="swiper-slide" style="background: url('/images/slide1.gif') no-repeat;">
              <div class="container">
                <div class="info">
                  <h1>Protection of architectural designs
                    as intellectual property</h1>
                    <a class="btn" href="">Xem thêm</a>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-pagination"></div>
      </div>
   </section>
   <section class="news-home">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-5 d-flex align-items-center">
          <div class="item">
            <h3>Cải tạo nhà phố cổ</h3>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>
            <a href="">Xem thêm <i class="fa fa-angle-right" aria-hidden="true"></i></a>
          </div>
        </div>
        <div class="col-lg-7 col-12">
          <a href="">
            <img src="/images/banner2.gif" alt="Cải tạo nhà phố cổ" width="100%">
          </a>
        </div>
      </div>
    </div>
   </section>

   <section class="news-home">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-7 col-12 @if($isMobile) order-1 @endif">
          <a href="">
            <img src="/images/banner2.gif" alt="Cải tạo nhà phố cổ" width="100%">
          </a>
        </div>
        <div class="col-lg-5 d-flex align-items-center">
          <div class="item">
            <h3>Cải tạo nhà phố cổ</h3>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>
            <a href="">Xem thêm <i class="fa fa-angle-right" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
    </div>
   </section>

  <section class="slide-service">
    <div class="swiper mySwiper">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <img src="/images/slide1.gif" width="100%" />
          <h3>Dự án nổi bật</h3>
        </div>
        <div class="swiper-slide">
          <img src="/images/banner2.gif" width="100%" />
          <h3>Dự án nổi bật</h3>
        </div>
        <div class="swiper-slide">
          <img src="/images/banner3.gif" width="100%" />
          <h3>Dự án nổi bật</h3>
        </div>
        <div class="swiper-slide">
          <img src="/images/slide1.gif" width="100%" />
          <h3>Dự án nổi bật</h3>
        </div>
        <div class="swiper-slide">
          <img src="/images/banner2.gif" width="100%" />
          <h3>Dự án nổi bật</h3>
        </div>
        <div class="swiper-slide">
          <img src="/images/banner3.gif" width="100%" />
          <h3>Dự án nổi bật</h3>
        </div>
      </div>
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
    </div>
  </section>

  <section class="blogs">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="blog-item-hlight">
            <a href="">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/OhPJSVskPa8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
              <h3>dự án biệt thự cao cấp</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </a>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="blog-item">
              <a href="">
                <img src="/images/banner2.gif" alt="" width="175px" height="135px">
                <div>
                  <h3>xu hướng kiến trúc của chung cư
                    cao cấp 2023</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
              </a>
            </div>
            <div class="blog-item">
              <a href="">
                <img src="/images/banner2.gif" alt="" width="175px" height="135px">
                <div>
                  <h3>xu hướng kiến trúc của chung cư
                    cao cấp 2023</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
              </a>
            </div>
            <div class="blog-item">
              <a href="">
                <img src="/images/banner2.gif" alt="" width="175px" height="135px">
                <div>
                  <h3>xu hướng kiến trúc của chung cư
                    cao cấp 2023</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
              </a>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('script')
<script type="text/javascript">
  jQuery(document).ready(function() {
    new Swiper(".slideSwiper", {
      pagination: {
        el: ".slideSwiper .swiper-pagination",
        clickable: true,
      },
      loop: true,
      autoplay: {
          delay: 3000,
          disableOnInteraction: false
      },
    });

    new Swiper(".mySwiper", {
      effect: "coverflow",
      slidesPerView: 3,
      loop: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      // autoplay: {
      //     delay: 3000,
      //     disableOnInteraction: false
      // },
    });
  });
</script>
@endsection
