@extends('website.layout.master')
@section('head')
    <link href="{{ url('home.css') }}" rel="stylesheet">
    <link href="{{ url('service.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section class="title-header" style="@if($isMobile == 1) background: url('/images/Hero_mb.png') no-repeat; @else background: url('/images/hero.png') no-repeat; @endif background-size: 100%;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center mr-auto" style="max-width: 770px">{!! $blog_categories->title !!}</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="cell-definition service-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 d-flex align-item-center justify-content-center">
                   <div>
                       <h1 class="title">{!! $blog_categories->short_description !!}</h1>
                       <img src="{{ isset($blog_categories->image) ? url('upload/'.$blog_categories->image) : '/images/tebaogoc.png' }}" class="d-block d-sm-none" width="100%">
                       <p>
                           {!! $blog_categories->description !!}
                       </p>
                   </div>
                </div>
                <div class="col-lg-6 col-md-6 d-none d-sm-block">
                    <img src="{{ isset($blog_categories->image) ? url('upload/'.$blog_categories->image) : '/images/tebaogoc.png' }}" width="100%">
                </div>
            </div>
        </div>
    </section>

    <section class="qt-service">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="title"><b>Quy trình</b> dịch vụ</h1>
                </div>
            </div>

            @if($isMobile == 1)
                <div class="swiper mySwiperService_detail">
                    <div class="swiper-wrapper">
                        @if(isset($blog_categories->serviceProcess))
                            @foreach($blog_categories->serviceProcess as $key => $serviceProcess)
                                <div class="swiper-slide">
                            <div class="item">
                                <div class="number">
                                    0{{ $key+1 }}
                                </div>
                                <img src="{{ isset($serviceProcess->image) ? url('upload/'.$serviceProcess->image) : '/images/Rectangle.png' }}" alt="{{ $serviceProcess->title }}" width="100%">
                                <p>{{ $serviceProcess->title }}</p>
                            </div>
                        </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            @else
                <div class="row">

                    <div class="col-lg-3">
                   <div class="item">
                       <div class="number">
                           01
                       </div>
                       <img src="/images/Rectangle.png" alt="01" width="100%">
                       <p>Tiếp nhận yêu cầu, bệnh án từ bệnh nhân</p>
                   </div>
                </div>

                </div>
            @endif
        </div>
    </section>

    <section class="ud-service service" style="background: linear-gradient(110.33deg, #E6F3FF -2.52%, rgba(246, 254, 254, 0.973543) 104.09%, rgba(231, 254, 250, 0.97) 133.69%);">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h1 class="title"><b>Ưu điểm</b> của dịch vụ</h1>
                </div>
                <div class="col-lg-8 d-flex justify-content-between align-items-center">
                    <ul>
                        <li>
                            <div class="icon-svg">
                                <svg width="36" height="37" viewBox="0 0 36 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect y="0.285645" width="36" height="36" rx="18" fill="url(#paint0_linear_867_37720)"></rect>
                                    <path d="M25.3334 12.7856L15.2501 22.869L10.6667 18.2856" stroke="#15457B" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <defs>
                                        <linearGradient id="paint0_linear_867_37720" x1="26.4779" y1="18.2567" x2="-12.5418" y2="1.44736" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#C3E3FD"></stop>
                                            <stop offset="0.87401" stop-color="#E7FEFA" stop-opacity="0.97"></stop>
                                        </linearGradient>
                                    </defs>
                                </svg>
                            </div>
                            <span>Dịch vụ tốt nhất với chi phí tối ưu nhất</span>
                        </li>
                        <li>
                            <div class="icon-svg">
                                <svg width="36" height="37" viewBox="0 0 36 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect y="0.285645" width="36" height="36" rx="18" fill="url(#paint0_linear_867_37720)"></rect>
                                    <path d="M25.3334 12.7856L15.2501 22.869L10.6667 18.2856" stroke="#15457B" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <defs>
                                        <linearGradient id="paint0_linear_867_37720" x1="26.4779" y1="18.2567" x2="-12.5418" y2="1.44736" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#C3E3FD"></stop>
                                            <stop offset="0.87401" stop-color="#E7FEFA" stop-opacity="0.97"></stop>
                                        </linearGradient>
                                    </defs>
                                </svg>
                            </div>
                            <span>Bác sĩ trực tiếp điều trị có kinh nghiệm với điều trị</span>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <div class="icon-svg">
                                <svg width="36" height="37" viewBox="0 0 36 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect y="0.285645" width="36" height="36" rx="18" fill="url(#paint0_linear_867_37720)"></rect>
                                    <path d="M25.3334 12.7856L15.2501 22.869L10.6667 18.2856" stroke="#15457B" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <defs>
                                        <linearGradient id="paint0_linear_867_37720" x1="26.4779" y1="18.2567" x2="-12.5418" y2="1.44736" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#C3E3FD"></stop>
                                            <stop offset="0.87401" stop-color="#E7FEFA" stop-opacity="0.97"></stop>
                                        </linearGradient>
                                    </defs>
                                </svg>
                            </div>
                            <span>Đối tác tại Nhật là các đơn vị uy tín, được kiểm chứng.</span>
                        </li>
                        <li>
                            <div class="icon-svg">
                                <svg width="36" height="37" viewBox="0 0 36 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect y="0.285645" width="36" height="36" rx="18" fill="url(#paint0_linear_867_37720)"></rect>
                                    <path d="M25.3334 12.7856L15.2501 22.869L10.6667 18.2856" stroke="#15457B" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <defs>
                                        <linearGradient id="paint0_linear_867_37720" x1="26.4779" y1="18.2567" x2="-12.5418" y2="1.44736" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#C3E3FD"></stop>
                                            <stop offset="0.87401" stop-color="#E7FEFA" stop-opacity="0.97"></stop>
                                        </linearGradient>
                                    </defs>
                                </svg>
                            </div>
                            <span>Dịch vụ đa dạng, đáp ứng mọi yêu cầu của khách hàng</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="contact-form">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h1 class="title"><b>Liên hệ</b> với chúng tôi</h1>
                    <p>Để được tư vấn về dịch vụ hoặc nhận thêm thông tin, vui lòng để lại thông tin liên hệ. Chúng tôi sẽ liên hệ ngay với bạn</p>
                    <img src="/images/lien-he.png" alt="contact" width="100%">
                </div>
                <div class="col-lg-6 col-md-6">
                    @include('website.common.form_register')
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            new Swiper(".mySwiperService_detail", {
                slidesPerView: "auto",
                slidesPerView: 1.2,
                spaceBetween: 30,
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
                breakpoints: {
                    // when window width is >= 320px
                    320: {
                        slidesPerView: 1.2,
                        spaceBetween: 20
                    }
                }
            });
        });
    </script>
@endsection

<style>
    .mr-auto {
        margin: auto;
    }

    .mySwiperService_detail {
        height: 400px;
    }

</style>
