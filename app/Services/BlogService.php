<?php

namespace App\Services;

use App\Models\Blog;

class BlogService
{
    protected $blogModel;

    public function __construct(Blog $blogModel)
    {
        $this->blogModel = $blogModel;
    }

    public function getBlogByPageIdAndCategoryId($page_id, $blogCategory)
    {
        $blogs = $this->blogModel->where([['page_id', $page_id], ['blog_category_id', $blogCategory], ['status', 'PUBLISHED']])->get();
        return $blogs;
    }

    public function getBlogByAlias($alias)
    {
        $blog = $this->blogModel->where('alias', $alias)->with('category')->first();
        return $blog;
    }
}
