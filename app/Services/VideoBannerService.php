<?php

namespace App\Services;

use App\Models\VideoBanner;

class VideoBannerService
{
    protected $videoBannerModel;

    public function __construct(VideoBanner $videoBannerModel)
    {
        $this->videoBannerModel = $videoBannerModel;
    }

    public function getVideoBannerByPageIdAndTypeFirst($page_id, $type)
    {
        $videoBanner = $this->videoBannerModel->where([['page_id', $page_id], ['type', $type]])->first();
        return $videoBanner;
    }

    public function getVideoBannerByPageIdAndTypeMultiple($page_id, $type)
    {
        $videoBanner = $this->videoBannerModel->where([['page_id', $page_id], ['type', $type]])->get();
        return $videoBanner;
    }

    public function getBannerContact($type)
    {
        $videoBanner = $this->videoBannerModel->where('type', $type)->first();
        return $videoBanner;
    }
}

