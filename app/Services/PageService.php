<?php

namespace App\Services;

use App\Models\Page;

class PageService
{
    protected $pageModel;

    public function __construct(Page $pageModel)
    {
        $this->pageModel = $pageModel;
    }

    public function getPageByAlias($alias)
    {
        $page = $this->pageModel->where('alias', $alias)->first();
        return $page;
    }
}
