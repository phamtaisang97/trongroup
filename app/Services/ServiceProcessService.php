<?php

namespace App\Services;

use App\Models\ServiceProcess;

class ServiceProcessService
{
    protected $serviceProcessModel;

    public function __construct(ServiceProcess $serviceProcessModel)
    {
        $this->serviceProcessModel = $serviceProcessModel;
    }

    public function getServiceProcessByBlogCategoryId($blog_category_id)
    {
        $serviceProcesses = $this->serviceProcessModel->where('blog_category_id', $blog_category_id)->get();
        return $serviceProcesses;
    }
}
