<?php

namespace App\Services;

use App\Models\Question;

class QuestionService
{
    protected $questionModel;

    public function __construct(Question $questionModel)
    {
        $this->questionModel = $questionModel;
    }

    public function getQuestionByPageId($page_id)
    {
        $questions = $this->questionModel->where('page_id', $page_id)->get();
        return $questions;
    }
}
