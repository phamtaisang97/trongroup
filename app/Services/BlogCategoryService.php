<?php

namespace App\Services;

use App\Models\BlogCategory;

class BlogCategoryService
{
    protected $blogCategoryModel;

    public function __construct(BlogCategory $blogCategoryModel)
    {
        $this->blogCategoryModel = $blogCategoryModel;
    }

    public function getBlogCategoryByPageIdByAlias($page_id, $alias)
    {
        $blogCategory = $this->blogCategoryModel->where([['page_id', $page_id], ['alias', $alias]])->first();
        return $blogCategory;
    }

    public function getBlogCategoryByPageIdFirst($page_id)
    {
        $blogCategory = $this->blogCategoryModel->where('page_id', $page_id)->first();
        return $blogCategory;
    }

    public function getBlogCategoriesByPageId($page_id)
    {
        $blogCategories = $this->blogCategoryModel->where('page_id', $page_id)->with(['blogs', 'serviceProcess'])->get();
        return $blogCategories;
    }

    public function getBlogCategoriesByAlias($alias)
    {
        $blogCategories = $this->blogCategoryModel->where('alias', $alias)->with(['blogs', 'serviceProcess'])->first();
        return $blogCategories;
    }
}
