<?php

namespace App\Services;

use App\Models\Partner;

class PartnerService
{
    protected $partnerModel;

    public function __construct(Partner $partnerModel)
    {
        $this->partnerModel = $partnerModel;
    }

    public function getPartnerByPageId($page_id)
    {
        $partners = $this->partnerModel->where('page_id', $page_id)->get();
        return $partners;
    }

    public function getPartners()
    {
        $partners = $this->partnerModel->get();
        return $partners;
    }
}
