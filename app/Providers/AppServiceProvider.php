<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        view()->composer('website/layout/master',function ($view){
            $menus = DB::table('page')->get();
            $common = DB::table('commons')->first();
            $view->with(compact('menus', 'common'));
        });
    }
}
