<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|max:255|unique:contact,email',
            'note' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => "Email không được để trống!",
            'email.unique' => "Email đã tồn tại trong hệ thống!",
            'email.max' => "Email không được quá 255 ký tự!",
            'note.required' => "Nội dung yêu cầu không được để trống!",
        ];
    }
}
