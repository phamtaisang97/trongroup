<?php

namespace App\Http\Controllers;

use App\Services\BlogService;
use App\Services\BlogCategoryService;
use App\Services\PageService;
use App\Services\VideoBannerService;

class HomeController extends Controller
{
    private $pageService;
    private $blogService;
    private $blogCategoryService;
    private $videoBannerService;

    public function __construct(PageService $pageService, BlogService $blogService, BlogCategoryService $blogCategoryService, VideoBannerService $videoBannerService)
    {
        $this->pageService = $pageService;
        $this->blogService = $blogService;
        $this->blogCategoryService = $blogCategoryService;
        $this->videoBannerService = $videoBannerService;
    }
    public function index()
    {
        $blogs = [];
        $questions = [];
        $page = $this->pageService->getPageByAlias("/");
        // if (isset($page['id'])) {
        //     $blogCategory_1 = $this->blogCategoryService->getBlogCategoryByPageIdByAlias($page['id'], 'hoat-dong-tieu-bieu');
        //     $blogs_1 = isset($blogCategory_1) ? $this->blogService->getBlogByPageIdAndCategoryId($page['id'], $blogCategory_1['id']) : [];
        //     $home_5 = $this->videoBannerService->getVideoBannerByPageIdAndTypeMultiple($page['id'], "home_5");
        //     $home_6 = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'], "home_6");
        //     $view_all_1 = isset($blogCategory_1) ? $blogCategory_1['alias'] : '';
        // }
        $isMobile = $this->isMobile();
        return view('website.home.index', compact('page', 'isMobile'));
    }

    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
