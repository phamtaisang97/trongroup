<?php

namespace App\Http\Controllers;

use App\Services\BlogCategoryService;
use App\Services\PageService;

class AboutController extends Controller
{
    private $pageService;
    private $blogCategoryService;

    public function __construct(PageService $pageService, BlogCategoryService $blogCategoryService)
    {
        $this->pageService = $pageService;
        $this->blogCategoryService = $blogCategoryService;
    }
    public function index()
    {
        $page = $this->pageService->getPageByAlias("gioi-thieu");
        if (isset($page['id'])) {
            $blogCategory = $this->blogCategoryService->getBlogCategoryByPageIdFirst($page['id']);
        }
        $isMobile = $this->isMobile();
        return view('website.about.index', compact('page', 'isMobile'));
    }

    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
