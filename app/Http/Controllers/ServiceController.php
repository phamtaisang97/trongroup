<?php

namespace App\Http\Controllers;

use App\Services\BlogCategoryService;
use App\Services\PageService;
use App\Services\ServiceProcessService;
use App\Services\VideoBannerService;

class ServiceController extends Controller
{
    private $pageService;
    private $videoBannerService;
    private $blogCategoryService;
    private $serviceProcessService;

    public function __construct(PageService $pageService, VideoBannerService $videoBannerService, BlogCategoryService $blogCategoryService, ServiceProcessService $serviceProcessService)
    {
        $this->pageService = $pageService;
        $this->videoBannerService = $videoBannerService;
        $this->blogCategoryService = $blogCategoryService;
        $this->serviceProcessService = $serviceProcessService;
    }
    public function index()
    {
        $page = $this->pageService->getPageByAlias("dich-vu");
        if (isset($page['id'])) {
            $blog_categories = $this->blogCategoryService->getBlogCategoriesByPageId($page['id']);
        }
        $isMobile = $this->isMobile();
        return view('website.service.index', compact('page','isMobile', 'blog_categories'));
    }

    public function detail($alias_category)
    {
        $blog_categories = $this->blogCategoryService->getBlogCategoriesByAlias($alias_category);
        $page = $blog_categories;
        $page['alias'] = 'dich-vu';
        $isMobile = $this->isMobile();
        return view('website.service.detail', compact('page', 'isMobile', 'blog_categories'));
    }

    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
