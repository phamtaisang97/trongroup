<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function register(ContactRequest $request)
    {
        try {
            $contact = new Contact();
            $contact->name = $request->name;
            $contact->email = $request->email;
            $contact->message = $request->note;

            $contact->save();

            return redirect(route('home'))->with('contactSuccess', 'Lời nhắn đã được gửi đi, Mirai Care cảm ơn bạn!');
        } catch (\Exception $exception) {
            return redirect(route('home'))->with('contactError', $exception->getMessage());
        }
    }
}
