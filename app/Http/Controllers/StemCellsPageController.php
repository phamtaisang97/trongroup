<?php

namespace App\Http\Controllers;

use App\Services\PageService;
use App\Services\BlogCategoryService;
use App\Services\BlogService;
use App\Services\QuestionService;
use App\Services\PartnerService;
use App\Services\VideoBannerService;

class StemCellsPageController extends Controller
{
    private $pageService;
    private $blogService;
    private $blogCategoryService;
    private $questionService;
    private $partnerService;
    private $videoBannerService;

    public function __construct(PageService $pageService, BlogService $blogService, BlogCategoryService $blogCategoryService, QuestionService $questionService, PartnerService $partnerService, VideoBannerService $videoBannerService)
    {
        $this->pageService = $pageService;
        $this->blogCategoryService = $blogCategoryService;
        $this->blogService = $blogService;
        $this->questionService = $questionService;
        $this->partnerService = $partnerService;
        $this->videoBannerService = $videoBannerService;
    }
    public function index()
    {
            /*"tbg_1" => '[TẾ BÀO GỐC] Liệu pháp tế bào gốc',
            "tbg_2" => '[TẾ BÀO GỐC] Tế bào gốc là nền tảng của sự sống',
            "tbg_3" => '[TẾ BÀO GỐC] Video youtube',
            "tbg_4" => '[TẾ BÀO GỐC] Quy trình điều trị chuyên biệt dựa trên tình trạng và mong muốn của khách hàng',
            "tbg_5" => '[TẾ BÀO GỐC] Dịch vụ tế bào gốc được tư vấn bởi Mirai Care',
            "tbg_6" => '[TẾ BÀO GỐC] Đặc quyền của khách hàng Mirai Care' */
          $blogs = [];
          $questions = [];
          $page = $this->pageService->getPageByAlias("lieu-phap-te-bao-goc");
          if (isset($page['id'])) {
              $blogCategory = $this->blogCategoryService->getBlogCategoryByPageIdByAlias($page['id'], 'ung-dung-lieu-phap-te-bao-goc');
              $blogs = isset($blogCategory) ? $this->blogService->getBlogByPageIdAndCategoryId($page['id'], $blogCategory['id']) : [];
              $questions = $this->questionService->getQuestionByPageId($page['id']);
              $tbg_1 = $this->videoBannerService->getVideoBannerByPageIdAndTypeMultiple($page['id'], "tbg_1");
              $tbg_2 = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'], "tbg_2");
              $tbg_3 = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'], "tbg_3");
              $tbg_4 = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'], "tbg_4");
              $tbg_5 = $this->videoBannerService->getVideoBannerByPageIdAndTypeMultiple($page['id'], "tbg_5");
              $tbg_6 = $this->videoBannerService->getVideoBannerByPageIdAndTypeMultiple($page['id'], "tbg_6");
              $contact_image = $this->videoBannerService->getBannerContact("contact");
          }
        $isMobile = $this->isMobile();
        $partners = $this->partnerService->getPartners();
          return view('website.stem_cells.index', compact('page', 'blogs', 'questions', 'partners', 'isMobile', 'tbg_1', 'tbg_2', 'tbg_3', 'tbg_4', 'tbg_5', 'tbg_6', 'contact_image'));
    }

    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
