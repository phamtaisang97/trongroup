<?php

namespace App\Http\Controllers;

use App\Services\BlogService;
use App\Services\PageService;

class BlogController extends Controller
{
    private $blogService;
    private $pageService;

    public function __construct(BlogService $blogService, PageService $pageService)
    {
        $this->blogService = $blogService;
        $this->pageService = $pageService;
    }

    public function index()
    {
        $blog_category = [];
        $blogs = [];//$this->blogService->getBlogByAlias($alias);
        $page = $this->pageService->getPageByAlias("tin-tuc");
        $isMobile = $this->isMobile();
        return view('website.blog.index', compact('page', 'blog_category', 'blogs', 'isMobile'));
    }

    public function project()
    {
        $blog_category = [];
        $blogs = [];//$this->blogService->getBlogByAlias($alias);
        $page = $this->pageService->getPageByAlias("gioi-thieu");
        $isMobile = $this->isMobile();
        return view('website.project.index', compact('page', 'blog_category', 'blogs', 'isMobile'));
    }

    public function detail($alias)
    {
        $blog = $this->blogService->getBlogByAlias($alias);
        $isMobile = $this->isMobile();
        if (isset($blog)) {
            $page = $blog;
            return view('website.blog.detail', compact('page', 'blog', 'isMobile'));
        }
        else {
            return view('website.404.index');
        }
    }

    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
