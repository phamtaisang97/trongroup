<?php

namespace App\Http\Controllers;

use App\Services\BlogService;
use App\Services\PageService;
use App\Services\PartnerService;
use App\Services\QuestionService;
use App\Services\VideoBannerService;
use Illuminate\Http\Request;

class ImmuneCellsPageController extends Controller
{
    private $pageService;
    private $questionService;
    private $partnerService;
    private $videoBannerService;
    const SLUG_NAME_IMMUNE_CELLS_PAGE = "te-bao-mien-dich";

    public function __construct(PageService $pageService,
                                QuestionService $questionService,
                                PartnerService $partnerService,
                                VideoBannerService $videoBannerService)
    {
        $this->pageService = $pageService;
        $this->questionService = $questionService;
        $this->partnerService = $partnerService;
        $this->videoBannerService = $videoBannerService;
    }
    public function index()
    {
        $questions = [];
        $videoHeader = [];
        $cellDefinitionImmune_1 = [];
        $cellDefinitionImmune_2 = [];
        $immuneCellMiraicare_3 = [];
        $immuneCellMiraicare_4 = [];
        $service_5 = [];
        $service_6 = [];

        $page = $this->pageService->getPageByAlias(self::SLUG_NAME_IMMUNE_CELLS_PAGE);
        $isMobile = $this->isMobile();

        if (!isset($page['id'])) {
            return view('website.immune_cells.index', compact('page', 'questions', 'isMobile', 'videoHeader',
                'cellDefinitionImmune_1', 'cellDefinitionImmune_2', 'immuneCellMiraicare_3', 'immuneCellMiraicare_4', 'service_5', 'service_6'));
        }

        $questions = $this->questionService->getQuestionByPageId($page['id']);
        $videoHeader = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'], "tbmg_0");
        $cellDefinitionImmune_1 = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'], "tbmg_1");
        $cellDefinitionImmune_2 = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'], "tbmg_2");
        $immuneCellMiraicare_3 = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'], "tbmg_01_02");
        $immuneCellMiraicare_4 = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'], "tbmg_03_04");
        $service_5 = $this->videoBannerService->getVideoBannerByPageIdAndTypeMultiple($page['id'], "tbmg_05");
        $service_6 = $this->videoBannerService->getVideoBannerByPageIdAndTypeMultiple($page['id'], "tbmg_06");
        $contact_image = $this->videoBannerService->getBannerContact("contact");
        $tbmg_07 = $this->videoBannerService->getVideoBannerByPageIdAndTypeFirst($page['id'],"tbmg_07");
        return view('website.immune_cells.index', compact('page', 'questions', 'isMobile', 'videoHeader',
            'cellDefinitionImmune_1', 'cellDefinitionImmune_2', 'immuneCellMiraicare_3', 'immuneCellMiraicare_4', 'service_5', 'service_6', 'contact_image', 'tbmg_07'));
    }

    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
