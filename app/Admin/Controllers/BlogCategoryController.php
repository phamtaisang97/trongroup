<?php

namespace App\Admin\Controllers;

use App\Models\BlogCategory;
use App\Models\Page;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class BlogCategoryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Danh mục bài viết';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BlogCategory());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Tiêu đề'));
        $grid->column('description', __('Mô tả'));
//        $grid->column('meta_title', __('Meta title'));
//        $grid->column('meta_description', __('Meta description'));
        $grid->column('alias', __('Alias'));
//        $grid->column('meta_keyword', __('Meta keyword'));
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BlogCategory::findOrFail($id));
        $show->field('title', __('Tiêu đề'));
        $show->field('description', __('Mô tả'));
        $show->field('meta_title', __('Meta title'));
        $show->field('meta_description', __('Meta description'));
        $show->field('alias', __('Alias'));
        $show->field('meta_keyword', __('Meta keyword'));
        $show->field('additional_contents', __('Additional contents'));
        $show->field('parent_id', __('Parent id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BlogCategory());

        $form->text('title', __('Tiêu đề'))->creationRules('required');
        $form->select('page_id', __('Page'))->options(Page::all()->pluck('title','id'));
        $form->ckeditor('description', __('Mô tả'));
        $form->textarea('short_description', __('Mô tả ngắn'));
        $form->text('meta_title', __('Meta title'));
        $form->textarea('meta_description', __('Meta description'));
        $form->text('alias', __('Alias'));
        $form->text('meta_keyword', __('Meta keyword'));
        $form->image('image', __('Hình ảnh'));
        $form->select('parent_id', __('Danh mục cha'))->options(BlogCategory::all()->pluck('title','id'));
        return $form;
    }
}
