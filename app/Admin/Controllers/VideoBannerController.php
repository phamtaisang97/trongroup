<?php

namespace App\Admin\Controllers;

use App\Models\Page;
use App\Models\VideoBanner;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class VideoBannerController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Video và Banner';
    const OPTION_TYPE_BANNER_VIDEO = [
        "home_1" => '[TRANG CHỦ][SLIDE] Đầu tiên',
        "home_2" => '[TRANG CHỦ][IMG,TEXT] Nội dung 1',
        "home_3" => '[TRANG CHỦ][IMG,TEXT] Nội dung 2',
        "home_4" => '[TRANG CHỦ][SLIDE, DU AN] SLide cho các dự án',
    ];

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new VideoBanner());

        // filter
        $grid->filter(function($filter){
            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->in('type', 'Màn hình')->multipleSelect(self::OPTION_TYPE_BANNER_VIDEO);
        });

        // data
        $grid->column('title', __('Tiêu đề'));
        $grid->column('description', __('Mô tả'));
        $grid->column('video', __('Video youtube'));
        $grid->column('banner', __('Banner'))->image();
        $grid->column('link', __('Link'));
        $grid->column('type', __('Loại'))->display(function () {
            return self::OPTION_TYPE_BANNER_VIDEO[$this->type];
        });
        $grid->column('page_id', __('Màn hình'))->display(function () {
            $page = Page::where('id', $this->page_id)->first();
            return $page->title;
        });
        $grid->model()->orderBy('id', 'desc');
        $grid->paginate(20);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(VideoBanner::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('video', __('Video youtube'));
        $show->field('banner', __('Banner'));
        $show->field('link', __('Link'));
        $show->field('page_id', __('Page'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new VideoBanner());
        $form->text('title', __('Tiêu đề'));
        $form->textarea('description', __('Mô tả'));
        $form->url('video', __('Video youtube'));
        $form->image('banner', __('Banner'));
        $form->url('link', __('Link'));
        $form->select('page_id', __('Page'))->options(Page::all()->pluck('title','id'))->creationRules('required');
        $form->select('type', __('Loại'))->options(self::OPTION_TYPE_BANNER_VIDEO)->creationRules('required');

        return $form;
    }
}
