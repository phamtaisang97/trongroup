<?php

namespace App\Admin\Controllers;

use App\Models\Common;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CommonController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Cài đặt chung';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Common());

        $grid->column('logo_header', __('Logo đầu trang'))->image();
        $grid->column('logo_footer', __('Logo chân trang'))->image();
        $grid->column('phone', __('Số điện thoại'));
        $grid->column('email', __('Email'));
        $grid->column('address', __('Địa chỉ'));
        $grid->column('link_facebook', __('Link facebook'));
        $grid->column('link_youtube', __('Link youtube'));
        $grid->column('link_google', __('Link google'));
        $grid->column('link_twitter', __('Link twitter'));
        $grid->column('link_linkedin', __('Link linkedin'));
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Common::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('logo_header', __('Logo đầu trang'));
        $show->field('logo_footer', __('Logo chân trang'));
        $show->field('phone', __('Số điện thoại'));
        $show->field('email', __('Email'));
        $show->field('address', __('Địa chỉ'));
        $show->field('link_facebook', __('Link facebook'));
        $show->field('link_youtube', __('Link youtube'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Common());

        $form->image('logo_header', __('Logo đầu trang'))->creationRules('required');
        $form->image('logo_footer', __('Logo chân trang'))->creationRules('required');
        $form->mobile('phone', __('Điện thoại'))->creationRules('required');
        $form->email('email', __('Email'));
        $form->text('address', __('Địa chỉ'));
        $form->text('link_facebook', __('Link facebook'));
        $form->text('link_youtube', __('Link youtube'));
        $form->text('link_google', __('Link google'));
        $form->text('link_twitter', __('Link twitter'));
        $form->text('link_linkedin', __('Link linkedin'));
        return $form;
    }
}
