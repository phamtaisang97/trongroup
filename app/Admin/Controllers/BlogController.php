<?php

namespace App\Admin\Controllers;

use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Page;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class BlogController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Bài viết';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Blog());

        // get data for filter
        $pages = Page::select('id', 'title')->get();
        $select_page = [];
        foreach ($pages as $itemPage) {
            if (empty($itemPage->id) || empty($itemPage->title)) {
                continue;
            }
            $select_page[$itemPage->id] = $itemPage->title;
        }

        $blog_categories = BlogCategory::select('id', 'title')->get();
        $select_blog_categories = [];
        foreach ($blog_categories as $itemBlogCat) {
            if (empty($itemBlogCat->id) || empty($itemBlogCat->title)) {
                continue;
            }
            $select_blog_categories[$itemBlogCat->id] = $itemBlogCat->title;
        }

        // filter
        $grid->filter(function($filter) use ($select_page, $select_blog_categories){
            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->in('blog_category_id', 'Danh mục')->multipleSelect($select_blog_categories);
            $filter->in('page_id', 'Màn hình')->multipleSelect($select_page);
        });

        // data
        $grid->column('blog_category_id', __('Danh mục'))->display(function () {
            $page = BlogCategory::where('id', $this->blog_category_id)->first();
            return isset($page->title) ? $page->title : '';
        });

        $grid->column('page_id', __('Màn hình'))->display(function () {
            $page = Page::where('id', $this->page_id)->first();
            return $page->title;
        });

        $grid->column('title', __('Tiêu đề'));
        $grid->column('image', __('hình ảnh'))->image("", 100, 100);
        $grid->column('video', __('Link video'));
        // $grid->column('description', __('Mô tả'));
        // $grid->column('short_description', __('Mô tả ngắn'));
        //$grid->column('meta_title', __('Meta title'));
        //$grid->column('meta_description', __('Meta description'));
        //$grid->column('alias', __('Alias'));
        //$grid->column('meta_keyword', __('Meta keyword'));
        $grid->column('status', __('Trạng thái'));
        $grid->column('created_at', __('Ngày tạo'))->display(function () {
            return date('d-m-Y H:i:s', strtotime($this->created_at));
        });
        $grid->column('updated_at', __('Ngày cập nhật'))->display(function () {
            return date('d-m-Y H:i:s', strtotime($this->updated_at));
        });;

        // quick search
        $grid->model()->orderBy('id', 'desc');
        $grid->quickSearch('title');

        $grid->paginate(20);
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Blog::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('blog_category_id', __('Blog category id'));
        $show->field('page_id', __('Page id'));
        $show->field('title', __('Tiêu đề'));
        $show->field('image', __('Hình ảnh'))->image();
        $show->field('video', __('Video'));
        $show->field('short_description', __('Mô tả ngắn'));
        $show->field('description', __('mô tả'));
        $show->field('meta_title', __('Meta title'));
        $show->field('meta_description', __('Meta description'));
        $show->field('alias', __('Alias'));
        $show->field('meta_keyword', __('Meta keyword'));
        $show->field('status', __('Trạng thái'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Blog());

        $form->select('blog_category_id', __('Danh mục bài viết'))->options(BlogCategory::all()->pluck('title','id'))->creationRules('required');
        $form->select('page_id', __('Page'))->options(Page::all()->pluck('title','id'));
        $form->text('title', __('Tiêu đề'))->creationRules('required');
        $form->image('image', __('Hình ảnh'))->creationRules('required');
        $form->text('video', __('Video'));
        $form->ckeditor('description', __('Mô tả'));
        $form->textarea('short_description', __('Mô tả ngắn'));
        $form->text('meta_title', __('Meta title'));
        $form->textarea('meta_description', __('Meta description'));
        $form->text('alias', __('Alias'))->creationRules('required');
        $form->text('meta_keyword', __('Meta keyword'));
        $form->switch('status', __('Trạng thái'))->default('PUBLISHED');

        return $form;
    }
}
