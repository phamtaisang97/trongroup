<?php

use App\Admin\Controllers\CommonController;
use App\Admin\Controllers\ContactController;
use App\Admin\Controllers\PageController;
use App\Admin\Controllers\BlogCategoryController;
use App\Admin\Controllers\BlogController;
use App\Admin\Controllers\VideoBannerController;
use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('common', CommonController::class);
    $router->resource('pages', PageController::class);
    $router->resource('blog-category', BlogCategoryController::class);
    $router->resource('blog', BlogController::class);
    $router->resource('video', VideoBannerController::class);
    $router->resource('contact', ContactController::class);
});


